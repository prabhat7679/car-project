function problem1(inventory, id){
    let arr=[];
    if(inventory === undefined ||  id === undefined || typeof inventory!=='object' || !Array.isArray(inventory) || typeof id !=='number' || inventory.length === 0){
        return arr;
    }
    for(let rowIndex=0; rowIndex<inventory.length; rowIndex++){
        if(inventory[rowIndex].id === id){
            arr=inventory[rowIndex];
        }
    }
    return arr;
} 

module.exports = problem1;
