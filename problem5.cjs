function problem5(inventory){
    let carYearArray=[];
    for(let index=0; index<inventory.length; index++){
        if(inventory[index].car_year < 2000){
            carYearArray.push(inventory[index].car_year);
        }
    }
    return carYearArray;
}   
 
module.exports=problem5;
