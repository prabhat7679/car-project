function problem4(inventory){
    // store all the car years
    let carYearArray=[];
    for(let index=0; index<inventory.length; index++){
        carYearArray.push(inventory[index].car_year);
    }
    return carYearArray;
}
module.exports=problem4;
