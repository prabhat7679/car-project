function problem3(inventory){
    // store the car model
    let carModel=[];
    for(let rowIndex=0; rowIndex<inventory.length; rowIndex++){
        let model=inventory[rowIndex].car_model;
        model=model.toUpperCase();
        carModel.push(model);
    }
    // sort the car model alphabetically
    
    return carModel.sort();
}
module.exports=problem3;
